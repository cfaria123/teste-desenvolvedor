﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoProxion
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }

        //Variáveis estáticas
        public static readonly string baseUrl = "https://prx-test.hasura.app/api/rest";
        public static string loggedUserName = String.Empty;
        public static string loggedFullName = String.Empty;
        public static int loggedUserId = 0;
    }
}
