﻿namespace ProjetoProxion
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.pWelcome = new System.Windows.Forms.Panel();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.lblRequiredText = new System.Windows.Forms.Label();
            this.lblUsernameRequired = new System.Windows.Forms.Label();
            this.lblPasswordRequired = new System.Windows.Forms.Label();
            this.llblRegisterSwitch = new System.Windows.Forms.LinkLabel();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblFullNameRequired = new System.Windows.Forms.Label();
            this.ttPassword = new System.Windows.Forms.ToolTip(this.components);
            this.pWelcome.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.White;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(64, 245);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(208, 63);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtUserName.Location = new System.Drawing.Point(64, 129);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(208, 26);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.Text = "Enter your Username...";
            this.txtUserName.TextChanged += new System.EventHandler(this.TxtUserName_TextChanged);
            this.txtUserName.Enter += new System.EventHandler(this.TxtUserName_Enter);
            this.txtUserName.Leave += new System.EventHandler(this.TxtUserName_Leave);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtPassword.Location = new System.Drawing.Point(64, 167);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(208, 26);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.Text = "Enter your Password...";
            this.txtPassword.TextChanged += new System.EventHandler(this.TxtPassword_TextChanged);
            this.txtPassword.Enter += new System.EventHandler(this.TxtPassword_Enter);
            this.txtPassword.Leave += new System.EventHandler(this.TxtPassword_Leave);
            // 
            // pWelcome
            // 
            this.pWelcome.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.pWelcome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pWelcome.Controls.Add(this.btnMinimize);
            this.pWelcome.Controls.Add(this.btnClose);
            this.pWelcome.Controls.Add(this.lblWelcome);
            this.pWelcome.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pWelcome.Location = new System.Drawing.Point(-1, -2);
            this.pWelcome.Name = "pWelcome";
            this.pWelcome.Size = new System.Drawing.Size(332, 69);
            this.pWelcome.TabIndex = 3;
            this.pWelcome.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PWelcome_MouseDown);
            this.pWelcome.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PWelcome_MouseMove);
            // 
            // btnMinimize
            // 
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.Location = new System.Drawing.Point(273, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(28, 26);
            this.btnMinimize.TabIndex = 2;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(303, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 26);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblWelcome.Location = new System.Drawing.Point(102, 21);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(134, 31);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome!";
            // 
            // lblRequiredText
            // 
            this.lblRequiredText.AutoSize = true;
            this.lblRequiredText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredText.Location = new System.Drawing.Point(100, 212);
            this.lblRequiredText.Name = "lblRequiredText";
            this.lblRequiredText.Size = new System.Drawing.Size(136, 13);
            this.lblRequiredText.TabIndex = 4;
            this.lblRequiredText.Text = "*All fields are required!";
            this.lblRequiredText.Visible = false;
            // 
            // lblUsernameRequired
            // 
            this.lblUsernameRequired.AutoSize = true;
            this.lblUsernameRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsernameRequired.Location = new System.Drawing.Point(272, 125);
            this.lblUsernameRequired.Name = "lblUsernameRequired";
            this.lblUsernameRequired.Size = new System.Drawing.Size(15, 20);
            this.lblUsernameRequired.TabIndex = 5;
            this.lblUsernameRequired.Text = "*";
            this.lblUsernameRequired.Visible = false;
            // 
            // lblPasswordRequired
            // 
            this.lblPasswordRequired.AutoSize = true;
            this.lblPasswordRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPasswordRequired.Location = new System.Drawing.Point(273, 164);
            this.lblPasswordRequired.Name = "lblPasswordRequired";
            this.lblPasswordRequired.Size = new System.Drawing.Size(15, 20);
            this.lblPasswordRequired.TabIndex = 6;
            this.lblPasswordRequired.Text = "*";
            this.lblPasswordRequired.Visible = false;
            // 
            // llblRegisterSwitch
            // 
            this.llblRegisterSwitch.AutoSize = true;
            this.llblRegisterSwitch.Location = new System.Drawing.Point(161, 314);
            this.llblRegisterSwitch.Name = "llblRegisterSwitch";
            this.llblRegisterSwitch.Size = new System.Drawing.Size(111, 13);
            this.llblRegisterSwitch.TabIndex = 7;
            this.llblRegisterSwitch.TabStop = true;
            this.llblRegisterSwitch.Text = "Click here to Register!";
            this.llblRegisterSwitch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LlblRegisterSwitch_LinkClicked);
            // 
            // txtFullName
            // 
            this.txtFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFullName.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtFullName.Location = new System.Drawing.Point(64, 91);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(208, 26);
            this.txtFullName.TabIndex = 8;
            this.txtFullName.Text = "Enter your Full Name...";
            this.txtFullName.Visible = false;
            this.txtFullName.TextChanged += new System.EventHandler(this.TxtFullName_TextChanged);
            this.txtFullName.Enter += new System.EventHandler(this.TxtFullName_Enter);
            this.txtFullName.Leave += new System.EventHandler(this.TxtFullName_Leave);
            // 
            // lblFullNameRequired
            // 
            this.lblFullNameRequired.AutoSize = true;
            this.lblFullNameRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullNameRequired.Location = new System.Drawing.Point(272, 87);
            this.lblFullNameRequired.Name = "lblFullNameRequired";
            this.lblFullNameRequired.Size = new System.Drawing.Size(15, 20);
            this.lblFullNameRequired.TabIndex = 9;
            this.lblFullNameRequired.Text = "*";
            this.lblFullNameRequired.Visible = false;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(330, 362);
            this.Controls.Add(this.lblFullNameRequired);
            this.Controls.Add(this.txtFullName);
            this.Controls.Add(this.llblRegisterSwitch);
            this.Controls.Add(this.lblPasswordRequired);
            this.Controls.Add(this.lblUsernameRequired);
            this.Controls.Add(this.lblRequiredText);
            this.Controls.Add(this.pWelcome);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LoginForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LoginForm_MouseMove);
            this.pWelcome.ResumeLayout(false);
            this.pWelcome.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Panel pWelcome;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblRequiredText;
        private System.Windows.Forms.Label lblUsernameRequired;
        private System.Windows.Forms.Label lblPasswordRequired;
        private System.Windows.Forms.LinkLabel llblRegisterSwitch;
        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.Label lblFullNameRequired;
        private System.Windows.Forms.ToolTip ttPassword;
    }
}

