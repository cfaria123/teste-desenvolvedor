﻿using ProjetoProxion.Model;
using ProjetoProxion.Rest;
using ProjetoProxion.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoProxion
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private bool creationMode = false; //Aplicação começa em modo de Login

        //Na tela de Login, os mesmos campos são utilizados para o Login e cadastro. O método abaixo gerencia essa funcionalidade
        private void switchCreationMode()
        {
            if(creationMode)
            {
                creationMode = false;

                txtFullName.Visible = false;
                btnLogin.Text = "Login";
                llblRegisterSwitch.Text = "Click here to Register!";
                resetFields();
            }
            else
            {
                creationMode = true;

                txtFullName.Visible = true;
                btnLogin.Text = "Register";
                llblRegisterSwitch.Text = "<- Return to Login!";
                resetFields();
            }
        }

        //Reseta todos os valores dos campos para seus valores originais
        private void resetFields()
        {
            txtFullName.Text = "Enter your Full Name...";
            txtPassword.Text = "Enter your Password...";
            txtUserName.Text = "Enter your Username...";
            txtPassword.PasswordChar = '\0';

            lblFullNameRequired.Visible = false;
            lblPasswordRequired.Visible = false;
            lblRequiredText.Visible = false;
            lblUsernameRequired.Visible = false;
        }
        //Botão de Login e Cadastro
        private async void btnLogin_Click(object sender, EventArgs e)
        {
            bool statusOk = true;

            if (txtUserName.Text.Trim() == String.Empty || txtUserName.Text == "Enter your Username...")
            {
                lblUsernameRequired.Visible = true;
                statusOk = false;
            }
            if (txtPassword.Text.Trim() == String.Empty || txtPassword.Text == "Enter your Password...")
            {
                lblPasswordRequired.Visible = true;
                statusOk = false;
            }
            if ((txtFullName.Text.Trim() == String.Empty || txtFullName.Text == "Enter your Full Name...") && creationMode)
            {
                lblFullNameRequired.Visible = true;
                statusOk = false;
            }
            if (statusOk) //Apenas prossegue se todas as condições estiverem OK
            {
                if(creationMode) //Em caso de uma criação, ou seja, creationMode true
                {
                    UserData user = await LoginHelper.GetUser(txtUserName.Text); //Verifica se o usuário já existe
                    if (txtPassword.Text.Length > 5) {
                        if (user == null) {
                            //Cadastra o usuário e retorna para o modo de Login
                            var response = await LoginHelper.AddUser(txtFullName.Text, txtUserName.Text, txtPassword.Text);
                            switchCreationMode();
                        }
                        else
                        {
                            MessageBox.Show("User already exists");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Password must have at least 6 characters");
                    }
                }
                else //Caso o modo de criação seja de Falso, ou seja, modo de Login
                {
                    //Faz o login e armazena os dados do usuário
                    var response = await LoginHelper.Login(txtUserName.Text, txtPassword.Text);

                    if (response != string.Empty) {
                        UserData user = await LoginHelper.GetUser(response);
                        Program.loggedFullName = user.full_name;
                        Program.loggedUserName = user.username;
                        Program.loggedUserId = user.id;

                        this.Hide();
                        resetFields();
                        MainForm main = new MainForm();
                        main.Show();
                    }
                }
            }
            else
            {
                lblRequiredText.Visible = true;
            }          
        }
        //Form Load faz ajustes na aparência do Form
        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();

            btnLogin.BackColor = Color.FromArgb(67, 166, 235);
            btnLogin.ForeColor = Color.White;

            txtUserName.BackColor = Color.FromArgb(248, 248, 248);
            txtPassword.BackColor = Color.FromArgb(248, 248, 248);

            lblUsernameRequired.ForeColor = Color.FromArgb(250, 69, 89);
            lblPasswordRequired.ForeColor = Color.FromArgb(250, 69, 89);
            lblFullNameRequired.ForeColor = Color.FromArgb(250, 69, 89);
            lblRequiredText.ForeColor = Color.FromArgb(250, 69, 89);
        }
        //Metodos para a funcionalidade do Placeholder Text
        #region PlaceHolder Text
        private void TxtUserName_Enter(object sender, EventArgs e)
        {
            if (txtUserName.Text == "Enter your Username...")
            {
                txtUserName.Text = "";
                lblUsernameRequired.Visible = false;
            }
        }

        private void TxtUserName_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtUserName.Text))
            {
                txtUserName.Text = "Enter your Username...";
            }
        }

        private void TxtPassword_Enter(object sender, EventArgs e)
        {
            if (txtPassword.Text == "Enter your Password...")
            {
                txtPassword.Text = "";
                txtPassword.PasswordChar = '*';
                lblPasswordRequired.Visible = false;
                if (creationMode)
                {
                    ttPassword.InitialDelay = 0;
                    ttPassword.IsBalloon = true;
                    ttPassword.Show(string.Empty, txtPassword);
                    ttPassword.Show("Password must have at least 6 characters", txtPassword,0);
                }
            }
        }

        private void TxtPassword_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                txtPassword.Text = "Enter your Password...";
                txtPassword.PasswordChar = '\0';
            }
        }
        private void TxtFullName_Enter(object sender, EventArgs e)
        {
            if (txtFullName.Text == "Enter your Full Name...")
            {
                txtFullName.Text = "";
                lblFullNameRequired.Visible = false;
            }
        }

        private void TxtFullName_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtFullName.Text))
            {
                txtFullName.Text = "Enter your Full Name...";
            }
        }

        #endregion

        //Metodos para as funcionalidades básicas do Form, como Drag, minimize e close
        #region Basic Functions
        private Point _mouseLoc;
        private void LoginForm_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseLoc = e.Location;
        }

        private void LoginForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.Location.X - _mouseLoc.X;
                int dy = e.Location.Y - _mouseLoc.Y;
                this.Location = new Point(this.Location.X + dx, this.Location.Y + dy);
            }
        }

        private void PWelcome_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseLoc = e.Location;
        }

        private void PWelcome_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.Location.X - _mouseLoc.X;
                int dy = e.Location.Y - _mouseLoc.Y;
                this.Location = new Point(this.Location.X + dx, this.Location.Y + dy);
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        #endregion

        //Botão que faz a troca entre modo de Login e cadastro
        private void LlblRegisterSwitch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            switchCreationMode();
        }
        //Limpa os avisos de campos obrigatórios
        #region Required Fields
        private void hideRequiredMessage()
        {
            if (creationMode)
            {
                if (txtFullName.Text != String.Empty && txtFullName.Text != "Enter your Full Name..." &&
                    txtUserName.Text != String.Empty && txtUserName.Text != "Enter your Username..." &&
                    txtPassword.Text != String.Empty && txtPassword.Text != "Enter your Password..."
                    )
                {
                    lblRequiredText.Visible = false;
                }
            }
            else
            {
                if (txtUserName.Text != String.Empty && txtUserName.Text != "Enter your Username..." &&
                    txtPassword.Text != String.Empty && txtPassword.Text != "Enter your Password..."
                    )
                {
                    lblRequiredText.Visible = false;
                }
            }
        }

        private void TxtFullName_TextChanged(object sender, EventArgs e)
        {
            hideRequiredMessage();
        }

        private void TxtUserName_TextChanged(object sender, EventArgs e)
        {
            hideRequiredMessage();
        }

        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
            hideRequiredMessage();
        }
        #endregion
    }
}
