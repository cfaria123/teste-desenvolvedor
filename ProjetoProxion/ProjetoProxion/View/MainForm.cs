﻿using ProjetoProxion.Model;
using ProjetoProxion.Rest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoProxion.View
{
    public partial class MainForm : Form
    {
        //Variáveis que mantém os dados das Listas e Tasks trazidas pela a API
        private Dictionary<int, string> taskLists = new Dictionary<int, string>();
        private List<KeyValuePair<int, Tuple<int, string>>> tasks = new List<KeyValuePair<int, Tuple<int, string>>>();
        public MainForm()
        {
            InitializeComponent();
        }
        //Metodos para as funcionalidades básicas do Form, como Drag, minimize e close
        #region Basic Functions
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private Point _mouseLoc;
        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseLoc = e.Location;
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.Location.X - _mouseLoc.X;
                int dy = e.Location.Y - _mouseLoc.Y;
                this.Location = new Point(this.Location.X + dx, this.Location.Y + dy);
            }
        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseLoc = e.Location;
        }

        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.Location.X - _mouseLoc.X;
                int dy = e.Location.Y - _mouseLoc.Y;
                this.Location = new Point(this.Location.X + dx, this.Location.Y + dy);
            }
        }
        #endregion
        //Faz o carregamento inicial das informações
        private void MainForm_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();

            lblWelcome.Text = "Welcome, " + Program.loggedFullName + "! Here are your tasks ↓";
            refresh();

            lTaskList.SelectedIndex = -1;
        }
        //Traz pela a API todos os dados cadastrados para Task List e Tasks e mostra na tela
        private async void refresh()
        {
            disabledWhileProcessing(true); //Desativa os botões enquanto espera pelo resposa
            //Limpa os dados dos listboxes
            taskLists.Clear();
            tasks.Clear();
            lTaskList.DataSource = null;
            lTasks.DataSource = null;

            List<getTaskListData> response = await MainHelper.GetAllTaskData();

            if (response != null)
            {
                foreach (var taskList in response)
                {
                    taskLists.Add(taskList.id, taskList.name);
                    lTaskList.DataSource = new BindingSource(taskLists, null);
                    lTaskList.DisplayMember = "Value";
                    lTaskList.ValueMember = "Key";
                    if (taskList.todo_list_items.Count > 0)
                    {
                        foreach (var task in taskList.todo_list_items)
                        {
                            string statusMessage = String.Empty;
                            if (task.is_done)
                            {
                                statusMessage = "Completed";
                            }
                            else
                            {
                                statusMessage = "On Going";
                            }
                            tasks.Add(new KeyValuePair<int, Tuple<int, string>>(
                                taskList.id, new Tuple<int, string>(task.id, task.name + " - " + statusMessage)));
                        }
                    }
                }
                loadTasks(); //Carrega os dados da Lista selecionada no Listbox de Tasks
            }
            disabledWhileProcessing(false); //Reativa os botões
        }
        //Faz Logout da tela Main e volta para a tela de Login
        private void BtnLogout_Click(object sender, EventArgs e)
        {
            Program.loggedFullName = String.Empty;
            Program.loggedUserName = String.Empty;
            Program.loggedUserId = 0;

            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Hide();
        }
        //Adiciona uma nova Task List
        private async void BtnAddTaskList_Click(object sender, EventArgs e)
        {
            if (txtTaskList.Text != String.Empty) {
                disabledWhileProcessing(true);
                //Faz o Post
                var response = await MainHelper.AddTaskList(txtTaskList.Text, Program.loggedUserId);
                taskLists.Add(response, txtTaskList.Text); //Adiciona nas Tasklists
                //Carrega o listbox
                lTaskList.DataSource = new BindingSource(taskLists, null);
                lTaskList.DisplayMember = "Value";
                lTaskList.ValueMember = "Key";
                disabledWhileProcessing(false);
            }
            else
            {
                MessageBox.Show("The Task name cannot be empty");
            }
        }
        //Desativa os botões enquanto a aplicação espera o response
        private void disabledWhileProcessing(bool processing)
        {
            if(processing)
            {
                btnAddTask.Enabled = false;
                btnAddTaskList.Enabled = false;
                btnEditTask.Enabled = false;
                btnEditTaskList.Enabled = false;
                btnRemoveTask.Enabled = false;
                btnRemoveTaskList.Enabled = false;
                lblLoad.Visible = true;
            }
            else
            {
                btnAddTask.Enabled = true;
                btnAddTaskList.Enabled = true;
                btnEditTask.Enabled = true;
                btnEditTaskList.Enabled = true;
                btnRemoveTask.Enabled = true;
                btnRemoveTaskList.Enabled = true;
                lblLoad.Visible = false;
            }
        }
        //Adiciona uma nova Task
        private async void BtnAddTask_Click(object sender, EventArgs e)
        {
            if (txtTask.Text != String.Empty)
            {
                if (lTaskList.SelectedIndex != -1) {
                    disabledWhileProcessing(true);
                    int selectedList = ((KeyValuePair<int, string>)lTaskList.SelectedItem).Key;
                    var response = await MainHelper.AddTask(txtTask.Text, selectedList);
                    tasks.Add(new KeyValuePair<int, Tuple<int, string>>(
                                selectedList, new Tuple<int, string>(response, txtTask.Text + " - " + "On Going")));

                    loadTasks();
                    disabledWhileProcessing(false);
                }
                else
                {
                    MessageBox.Show("A Task List must be selected to add a new Task");
                }
            }
            else
            {
                MessageBox.Show("The Task name cannot be empty");
            }
        }
        //Carrega o nome da Task List e carrega as Tasks ao trocar de Task List
        private void LTaskList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lTaskList.SelectedIndex != -1)
            {
                loadTasks();
                txtTaskList.Text = ((KeyValuePair<int, string>)lTaskList.SelectedItem).Value;
            }
        }
        //Carrega as Tasks da Tasklist selecionada
        private void loadTasks()
        {
            Dictionary<int, string> tasksToAdd = new Dictionary<int, string>();
            int selectedList = ((KeyValuePair<int, string>)lTaskList.SelectedItem).Key;
            foreach (var task in tasks)
            {
                if (task.Key == selectedList)
                {
                    tasksToAdd.Add(task.Value.Item1, task.Value.Item2);
                }
            }
            if (tasksToAdd.Count > 0) { 
                lTasks.DataSource = new BindingSource(tasksToAdd, null);
                lTasks.DisplayMember = "Value";
                lTasks.ValueMember = "Key";
                txtTask.Enabled = true;
            }
            else
            {
                lTasks.DataSource = null;
            }
        }
        //Carrega as informações da Tax no ComboBox e na caixxa de Texto
        private void LTasks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lTasks.SelectedIndex != -1) {
                string selectedTaskValue = ((KeyValuePair<int, string>)lTasks.SelectedItem).Value;
                var status = selectedTaskValue.Substring(selectedTaskValue.LastIndexOf('-') + 1).Trim();
                cmbTaskStatus.SelectedItem = status;
                var name = selectedTaskValue.Substring(0, selectedTaskValue.LastIndexOf('-') - 1).Trim();
                txtTask.Text = name;
            }
            else
            {
                cmbTaskStatus.SelectedIndex = -1;
                txtTask.Text = String.Empty;
            }
        }
        //Edita o nome e status da Task
        private async void BtnEditTask_Click(object sender, EventArgs e)
        {
            if (lTasks.SelectedIndex != -1)
            {
                if (txtTask.Text != String.Empty)
                {
                    disabledWhileProcessing(true);
                    int taskListId = ((KeyValuePair<int, string>)lTaskList.SelectedItem).Key;
                    int taskId = ((KeyValuePair<int, string>)lTasks.SelectedItem).Key;
                    bool status;
                    if (cmbTaskStatus.Text == "Completed")
                    {
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                    var response = await MainHelper.EditTask(taskListId, txtTask.Text, taskId, status);

                    refresh();
                    disabledWhileProcessing(false);
                }
                else
                {
                    MessageBox.Show("A Task name is required");
                }
            }
            else
            {
                MessageBox.Show("A Task must be selected");
            }
        }
        //Remove uma Task
        private async void BtnRemoveTask_Click(object sender, EventArgs e)
        {
            if(lTasks.SelectedIndex != -1)
            {
                DialogResult dialogChoice = MessageBox.Show("Are you sure you want to delete this Task?", "Task deletion", MessageBoxButtons.YesNo);
                if (dialogChoice == DialogResult.Yes)
                {
                    disabledWhileProcessing(true);
                    var response = await MainHelper.RemoveTask(((KeyValuePair<int, string>)lTasks.SelectedItem).Key);
                    refresh();
                    disabledWhileProcessing(false);
                }
            }
            else
            {
                MessageBox.Show("A task must be selected");
            }
        }
        //Remove uma Task List
        private async void BtnRemoveTaskList_Click(object sender, EventArgs e)
        {
            if (lTaskList.SelectedIndex != -1)
            {
                DialogResult dialogChoice = MessageBox.Show("Are you sure you want to delete this Task List?", "Task deletion", MessageBoxButtons.YesNo);
                if (dialogChoice == DialogResult.Yes)
                {
                    disabledWhileProcessing(true);
                    var response = await MainHelper.RemoveTaskList(((KeyValuePair<int, string>)lTaskList.SelectedItem).Key);
                    refresh();
                    if(lTaskList.SelectedIndex == -1)
                    {
                        txtTaskList.Text = String.Empty;
                    }
                    disabledWhileProcessing(false);
                }
            }
            else
            {
                MessageBox.Show("A Task List must be selected");
            }
        }
        //Edita o nome da Task List
        private async void BtnEditTaskList_Click(object sender, EventArgs e)
        {
            if (lTaskList.SelectedIndex != -1)
            {
                if (txtTaskList.Text != String.Empty)
                {
                    disabledWhileProcessing(true);
                    int taskListId = ((KeyValuePair<int, string>)lTaskList.SelectedItem).Key;
                    var response = await MainHelper.EditTaskList(taskListId, txtTaskList.Text, Program.loggedUserId);

                    refresh();
                    disabledWhileProcessing(false);
                }
                else
                {
                    MessageBox.Show("A Task List name is required");
                }
            }
            else
            {
                MessageBox.Show("A Task List must be selected");
            }
        }
    }
}
