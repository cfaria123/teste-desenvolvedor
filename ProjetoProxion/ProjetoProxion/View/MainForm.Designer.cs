﻿namespace ProjetoProxion.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.lTaskList = new System.Windows.Forms.ListBox();
            this.lTasks = new System.Windows.Forms.ListBox();
            this.txtTaskList = new System.Windows.Forms.TextBox();
            this.txtTask = new System.Windows.Forms.TextBox();
            this.btnAddTaskList = new System.Windows.Forms.Button();
            this.cmbTaskStatus = new System.Windows.Forms.ComboBox();
            this.btnEditTaskList = new System.Windows.Forms.Button();
            this.btnRemoveTaskList = new System.Windows.Forms.Button();
            this.btnRemoveTask = new System.Windows.Forms.Button();
            this.btnEditTask = new System.Windows.Forms.Button();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.lblTaskList = new System.Windows.Forms.Label();
            this.lblTasks = new System.Windows.Forms.Label();
            this.lblListName = new System.Windows.Forms.Label();
            this.lblTaskName = new System.Windows.Forms.Label();
            this.lblTaskStatus = new System.Windows.Forms.Label();
            this.lblLoad = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMinimize
            // 
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.Location = new System.Drawing.Point(743, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(28, 26);
            this.btnMinimize.TabIndex = 4;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(773, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(28, 26);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnLogout);
            this.panel1.Controls.Add(this.lblWelcome);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnMinimize);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 54);
            this.panel1.TabIndex = 5;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel1_MouseMove);
            // 
            // btnLogout
            // 
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnLogout.Location = new System.Drawing.Point(683, 0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(57, 26);
            this.btnLogout.TabIndex = 6;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblWelcome.Location = new System.Drawing.Point(12, 18);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(288, 24);
            this.lblWelcome.TabIndex = 5;
            this.lblWelcome.Text = "Welcome, ! Here are your tasks ↓";
            // 
            // lTaskList
            // 
            this.lTaskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTaskList.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lTaskList.FormattingEnabled = true;
            this.lTaskList.HorizontalScrollbar = true;
            this.lTaskList.ItemHeight = 16;
            this.lTaskList.Location = new System.Drawing.Point(139, 94);
            this.lTaskList.Name = "lTaskList";
            this.lTaskList.Size = new System.Drawing.Size(238, 260);
            this.lTaskList.TabIndex = 6;
            this.lTaskList.SelectedIndexChanged += new System.EventHandler(this.LTaskList_SelectedIndexChanged);
            // 
            // lTasks
            // 
            this.lTasks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTasks.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lTasks.FormattingEnabled = true;
            this.lTasks.HorizontalScrollbar = true;
            this.lTasks.ItemHeight = 16;
            this.lTasks.Location = new System.Drawing.Point(418, 94);
            this.lTasks.Name = "lTasks";
            this.lTasks.Size = new System.Drawing.Size(238, 228);
            this.lTasks.TabIndex = 7;
            this.lTasks.SelectedIndexChanged += new System.EventHandler(this.LTasks_SelectedIndexChanged);
            // 
            // txtTaskList
            // 
            this.txtTaskList.Location = new System.Drawing.Point(221, 366);
            this.txtTaskList.Name = "txtTaskList";
            this.txtTaskList.Size = new System.Drawing.Size(156, 20);
            this.txtTaskList.TabIndex = 8;
            // 
            // txtTask
            // 
            this.txtTask.Location = new System.Drawing.Point(500, 338);
            this.txtTask.Name = "txtTask";
            this.txtTask.Size = new System.Drawing.Size(156, 20);
            this.txtTask.TabIndex = 9;
            // 
            // btnAddTaskList
            // 
            this.btnAddTaskList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnAddTaskList.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnAddTaskList.FlatAppearance.BorderSize = 0;
            this.btnAddTaskList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddTaskList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnAddTaskList.Location = new System.Drawing.Point(139, 392);
            this.btnAddTaskList.Name = "btnAddTaskList";
            this.btnAddTaskList.Size = new System.Drawing.Size(75, 23);
            this.btnAddTaskList.TabIndex = 12;
            this.btnAddTaskList.Text = "Add List";
            this.btnAddTaskList.UseVisualStyleBackColor = false;
            this.btnAddTaskList.Click += new System.EventHandler(this.BtnAddTaskList_Click);
            // 
            // cmbTaskStatus
            // 
            this.cmbTaskStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTaskStatus.FormattingEnabled = true;
            this.cmbTaskStatus.Items.AddRange(new object[] {
            "Completed",
            "On Going"});
            this.cmbTaskStatus.Location = new System.Drawing.Point(500, 364);
            this.cmbTaskStatus.Name = "cmbTaskStatus";
            this.cmbTaskStatus.Size = new System.Drawing.Size(156, 21);
            this.cmbTaskStatus.TabIndex = 13;
            // 
            // btnEditTaskList
            // 
            this.btnEditTaskList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnEditTaskList.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnEditTaskList.FlatAppearance.BorderSize = 0;
            this.btnEditTaskList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditTaskList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnEditTaskList.Location = new System.Drawing.Point(221, 392);
            this.btnEditTaskList.Name = "btnEditTaskList";
            this.btnEditTaskList.Size = new System.Drawing.Size(75, 23);
            this.btnEditTaskList.TabIndex = 14;
            this.btnEditTaskList.Text = "Edit List";
            this.btnEditTaskList.UseVisualStyleBackColor = false;
            this.btnEditTaskList.Click += new System.EventHandler(this.BtnEditTaskList_Click);
            // 
            // btnRemoveTaskList
            // 
            this.btnRemoveTaskList.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnRemoveTaskList.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnRemoveTaskList.FlatAppearance.BorderSize = 0;
            this.btnRemoveTaskList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveTaskList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnRemoveTaskList.Location = new System.Drawing.Point(302, 392);
            this.btnRemoveTaskList.Name = "btnRemoveTaskList";
            this.btnRemoveTaskList.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveTaskList.TabIndex = 15;
            this.btnRemoveTaskList.Text = "Delete List";
            this.btnRemoveTaskList.UseVisualStyleBackColor = false;
            this.btnRemoveTaskList.Click += new System.EventHandler(this.BtnRemoveTaskList_Click);
            // 
            // btnRemoveTask
            // 
            this.btnRemoveTask.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnRemoveTask.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnRemoveTask.FlatAppearance.BorderSize = 0;
            this.btnRemoveTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveTask.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnRemoveTask.Location = new System.Drawing.Point(581, 392);
            this.btnRemoveTask.Name = "btnRemoveTask";
            this.btnRemoveTask.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveTask.TabIndex = 18;
            this.btnRemoveTask.Text = "Delete Task";
            this.btnRemoveTask.UseVisualStyleBackColor = false;
            this.btnRemoveTask.Click += new System.EventHandler(this.BtnRemoveTask_Click);
            // 
            // btnEditTask
            // 
            this.btnEditTask.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnEditTask.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnEditTask.FlatAppearance.BorderSize = 0;
            this.btnEditTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditTask.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnEditTask.Location = new System.Drawing.Point(500, 392);
            this.btnEditTask.Name = "btnEditTask";
            this.btnEditTask.Size = new System.Drawing.Size(75, 23);
            this.btnEditTask.TabIndex = 17;
            this.btnEditTask.Text = "Edit Task";
            this.btnEditTask.UseVisualStyleBackColor = false;
            this.btnEditTask.Click += new System.EventHandler(this.BtnEditTask_Click);
            // 
            // btnAddTask
            // 
            this.btnAddTask.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btnAddTask.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnAddTask.FlatAppearance.BorderSize = 0;
            this.btnAddTask.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddTask.ForeColor = System.Drawing.SystemColors.MenuText;
            this.btnAddTask.Location = new System.Drawing.Point(418, 392);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(75, 23);
            this.btnAddTask.TabIndex = 16;
            this.btnAddTask.Text = "Add Task";
            this.btnAddTask.UseVisualStyleBackColor = false;
            this.btnAddTask.Click += new System.EventHandler(this.BtnAddTask_Click);
            // 
            // lblTaskList
            // 
            this.lblTaskList.AutoSize = true;
            this.lblTaskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaskList.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblTaskList.Location = new System.Drawing.Point(135, 67);
            this.lblTaskList.Name = "lblTaskList";
            this.lblTaskList.Size = new System.Drawing.Size(80, 20);
            this.lblTaskList.TabIndex = 7;
            this.lblTaskList.Text = "Task Lists";
            // 
            // lblTasks
            // 
            this.lblTasks.AutoSize = true;
            this.lblTasks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTasks.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblTasks.Location = new System.Drawing.Point(414, 67);
            this.lblTasks.Name = "lblTasks";
            this.lblTasks.Size = new System.Drawing.Size(51, 20);
            this.lblTasks.TabIndex = 19;
            this.lblTasks.Text = "Tasks";
            // 
            // lblListName
            // 
            this.lblListName.AutoSize = true;
            this.lblListName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblListName.Location = new System.Drawing.Point(136, 367);
            this.lblListName.Name = "lblListName";
            this.lblListName.Size = new System.Drawing.Size(71, 17);
            this.lblListName.TabIndex = 20;
            this.lblListName.Text = "List Name";
            // 
            // lblTaskName
            // 
            this.lblTaskName.AutoSize = true;
            this.lblTaskName.BackColor = System.Drawing.Color.White;
            this.lblTaskName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaskName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblTaskName.Location = new System.Drawing.Point(415, 339);
            this.lblTaskName.Name = "lblTaskName";
            this.lblTaskName.Size = new System.Drawing.Size(80, 17);
            this.lblTaskName.TabIndex = 21;
            this.lblTaskName.Text = "Task Name";
            // 
            // lblTaskStatus
            // 
            this.lblTaskStatus.AutoSize = true;
            this.lblTaskStatus.BackColor = System.Drawing.Color.White;
            this.lblTaskStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaskStatus.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblTaskStatus.Location = new System.Drawing.Point(415, 365);
            this.lblTaskStatus.Name = "lblTaskStatus";
            this.lblTaskStatus.Size = new System.Drawing.Size(83, 17);
            this.lblTaskStatus.TabIndex = 22;
            this.lblTaskStatus.Text = "Task Status";
            // 
            // lblLoad
            // 
            this.lblLoad.AutoSize = true;
            this.lblLoad.BackColor = System.Drawing.Color.Transparent;
            this.lblLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoad.Location = new System.Drawing.Point(13, 425);
            this.lblLoad.Name = "lblLoad";
            this.lblLoad.Size = new System.Drawing.Size(78, 20);
            this.lblLoad.TabIndex = 23;
            this.lblLoad.Text = "Loading...";
            this.lblLoad.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblLoad);
            this.Controls.Add(this.lblTaskStatus);
            this.Controls.Add(this.lblTaskName);
            this.Controls.Add(this.lblListName);
            this.Controls.Add(this.lblTasks);
            this.Controls.Add(this.lblTaskList);
            this.Controls.Add(this.btnRemoveTask);
            this.Controls.Add(this.btnEditTask);
            this.Controls.Add(this.btnAddTask);
            this.Controls.Add(this.btnRemoveTaskList);
            this.Controls.Add(this.btnEditTaskList);
            this.Controls.Add(this.cmbTaskStatus);
            this.Controls.Add(this.btnAddTaskList);
            this.Controls.Add(this.txtTask);
            this.Controls.Add(this.txtTaskList);
            this.Controls.Add(this.lTasks);
            this.Controls.Add(this.lTaskList);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.ListBox lTaskList;
        private System.Windows.Forms.ListBox lTasks;
        private System.Windows.Forms.TextBox txtTaskList;
        private System.Windows.Forms.TextBox txtTask;
        private System.Windows.Forms.Button btnAddTaskList;
        private System.Windows.Forms.ComboBox cmbTaskStatus;
        private System.Windows.Forms.Button btnEditTaskList;
        private System.Windows.Forms.Button btnRemoveTaskList;
        private System.Windows.Forms.Button btnRemoveTask;
        private System.Windows.Forms.Button btnEditTask;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.Label lblTaskList;
        private System.Windows.Forms.Label lblTasks;
        private System.Windows.Forms.Label lblListName;
        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.Label lblTaskStatus;
        private System.Windows.Forms.Label lblLoad;
    }
}