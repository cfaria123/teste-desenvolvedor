﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoProxion.Model
{
    #region GetUserModel
    public class UserRootObject
    {
        public List<UserData> user;
    }
    public class UserModel
    {
        public UserData user { get; set; }
    }
    public class UserData
    {
        public string username { get; set; }
        public string password { get; set; }
        public string full_name { get; set; }
        public int id { get; set; }
    }
    #endregion
    #region Add User Model
    public class AddUserModel
    {
        public AddUserData user { get; set; }
    }
    public class AddUserData
    {
        public string username { get; set; }
        public string password { get; set; }
        public string full_name { get; set; }
    }
    #endregion
}
