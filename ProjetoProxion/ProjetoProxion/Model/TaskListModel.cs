﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoProxion.Model
{
    #region Add Task Lists
    public class TaskListModel
    {
        public TaskListData todo_list { get; set; }
    }
    public class TaskListData
    {
        public string name { get; set; }
        public int user_id { get; set; }
    }
    #endregion
    #region Add Task List Return Model
    public class TaskListAddRootObject
    {
        public TaskListAddReturnData insert_todo_list_one;
    }
    public class TaskListAddReturnData
    {
        public int id { get; set; }
    }
    #endregion
    #region Get All Task Lists and Tasks Data
    public class getTaskListDataRootObject
    {
        public List<getTaskListData> todo_list;
    }
    public class getTaskListData
    {
        public int id { get; set; }
        public string name { get; set; }

        public List<getTaskData> todo_list_items;
    }
    public class getTaskData
    {
        public int id { get; set; }

        public string name { get; set; }

        public bool is_done { get; set; }
    }
    #endregion
    #region Add Task to List
    public class AddTaskModel
    {
        public AddTaskData item { get; set; }
    }
    public class AddTaskData
    {
        public string name { get; set; }
        public int todo_list_id { get; set; }
    }
    #endregion
    #region Add Task to List Return Model
    public class AddTaskReturnRootObject
    {
        public AddTaskReturnData insert_todo_list_item_one { get; set; }
    }
    public class AddTaskReturnData
    {
        public int id { get; set; }
    }
    #endregion
    #region Edit Task Model
    public class EditTaskModel
    {
        public int id { get; set; }
        public EditTaskData item { get; set; }
}
    public class EditTaskData
    {
        public string name { get; set; }

        public int todo_list_id { get; set; }

        public bool is_done { get; set; }
    }
    #endregion
    #region Edit Task List Model
    public class EditTaskListModel
    {
        public int id { get; set; }
        public EditTaskListData todo_list { get; set; }
    }
    public class EditTaskListData
    {
        public string name { get; set; }
        public int user_id { get; set; }
    }
    #endregion
}
