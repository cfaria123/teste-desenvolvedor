﻿using ProjetoProxion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ProjetoProxion.Rest
{
    public static class MainHelper
    {
        

        //Traz todos os dados de Task List e Task para a Tela Main
        public static async Task<List<getTaskListData>> GetAllTaskData()
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage resp = await client.GetAsync(Program.baseUrl + "/todo/get/" + Program.loggedUserId))
                {
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        getTaskListDataRootObject taskListData = new JavaScriptSerializer().Deserialize<getTaskListDataRootObject>(data);

                        if (taskListData.todo_list.Count > 0)
                        {
                            return taskListData.todo_list;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        #region Task API Stuff
        //Adiciona uma nova Task
        public static async Task<int> AddTask(string name, int todo_list_id)
        {
            using (HttpClient client = new HttpClient())
            {
                AddTaskModel newTaskList = new AddTaskModel();
                newTaskList.item = new AddTaskData { name = name, todo_list_id = todo_list_id };

                var json = new JavaScriptSerializer().Serialize(newTaskList);
                var input = new StringContent(json, Encoding.UTF8, "application/json");

                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/todo/item/add", input))
                {
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        MessageBox.Show("An error ocurred during the Task registration");
                    }
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        AddTaskReturnRootObject returnData = new JavaScriptSerializer().Deserialize<AddTaskReturnRootObject>(data);
                        return returnData.insert_todo_list_item_one.id;
                    }
                }
            }
        }
        //Edita o nome e o Status da Task
        public static async Task<string> EditTask(int list_id, string name, int task_id, bool isDone)
        {
            using (HttpClient client = new HttpClient())
            {
                EditTaskModel newTaskList = new EditTaskModel();
                newTaskList.id = task_id;
                newTaskList.item = new EditTaskData { name = name, todo_list_id = list_id, is_done = isDone };

                var json = new JavaScriptSerializer().Serialize(newTaskList);
                var input = new StringContent(json, Encoding.UTF8, "application/json");
                
                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/todo/item/edit", input))
                {
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        MessageBox.Show("An error ocurred during the Task update");
                    }
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        return data;
                    }
                }
            }
        }
        //Remover a Task. Mesmo em Tasks com ID existente a API sempre retorna um 404. Prints: https://imgur.com/a/OkhOHzL
        public static async Task<string> RemoveTask(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage resp = await client.DeleteAsync(Program.baseUrl + "/todo/item/delete/" + id))
                {
                    using (HttpContent content = resp.Content)
                    {
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            MessageBox.Show("Olá pessoal! Creio que algo esteja errado com esse Delete. O de List funcionou normalmente, mas esse sempre me retorna 404 mesmo quando o ID existe. Perdão se eu estiver enganado. Segue link com prints: https://imgur.com/a/OkhOHzL");
                        }
                        var data = await content.ReadAsStringAsync();
                        return data;

                    
                    }
                }
            }
        }
        #endregion
        #region Task List API Stuff
        //Adiciona uma nova Task List
        public static async Task<int> AddTaskList(string name, int user_id)
        {
            using (HttpClient client = new HttpClient())
            {
                TaskListModel newTaskList = new TaskListModel();
                newTaskList.todo_list = new TaskListData { name = name, user_id = user_id };

                var json = new JavaScriptSerializer().Serialize(newTaskList);
                var input = new StringContent(json, Encoding.UTF8, "application/json");

                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/todo/add", input))
                {
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        MessageBox.Show("An error ocurred during the Task List registration");
                    }
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        TaskListAddRootObject returnData = new JavaScriptSerializer().Deserialize<TaskListAddRootObject>(data);
                        return returnData.insert_todo_list_one.id;
                    }
                }
            }
        }
        //Remove uma Task List
        public static async Task<string> RemoveTaskList(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage resp = await client.DeleteAsync(Program.baseUrl + "/todo/delete/" + id))
                {
                    using (HttpContent content = resp.Content)
                    {
                        if (resp.StatusCode != HttpStatusCode.OK)
                        {
                            MessageBox.Show("An error ocurred during Task List removal");
                        }
                        var data = await content.ReadAsStringAsync();
                        return data;
                    }
                }
            }
        }
        //Edita o nome de uma Task List
        public static async Task<string> EditTaskList(int id, string name, int user_id)
        {
            using (HttpClient client = new HttpClient())
            {
                EditTaskListModel newTaskList = new EditTaskListModel();
                newTaskList.id = id;
                newTaskList.todo_list = new EditTaskListData { name = name, user_id = user_id };

                var json = new JavaScriptSerializer().Serialize(newTaskList);
                var input = new StringContent(json, Encoding.UTF8, "application/json");

                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/todo/edit", input))
                {
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        MessageBox.Show("An error ocurred during the Task List update");
                    }
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        return data;
                    }
                }
            }
        }
        #endregion
    }
}
