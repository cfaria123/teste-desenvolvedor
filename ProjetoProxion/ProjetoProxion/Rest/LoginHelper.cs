﻿using ProjetoProxion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ProjetoProxion.Rest
{
    public static class LoginHelper
    {
        //Post na API para cadastrar um novo usuário
        public static async Task<string> AddUser(string fullName, string userName, string passWord)
        {
            using (HttpClient client = new HttpClient())
            {
                AddUserModel newUser = new AddUserModel();
                newUser.user = new AddUserData { username = userName, password = Encrypt(passWord), full_name = fullName };

                var json = new JavaScriptSerializer().Serialize(newUser);
                var input = new StringContent(json, Encoding.UTF8, "application/json");

                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/user/add", input))
                {
                    if(resp.StatusCode == HttpStatusCode.OK)
                    {
                        MessageBox.Show("Account registered! Please continue with your first Login!");
                    }
                    else
                    {
                        MessageBox.Show("An error ocurred during the registration");
                    }
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        return data;
                    }
                }
            }
        }
        //Post na API para fazer o Login do usuário
        public static async Task<string> Login(string userName, string passWord)
        {
            using (HttpClient client = new HttpClient())
            {
                Dictionary<string, string> loginData = new Dictionary<string, string>
                {
                    { "username", userName },
                    { "password", Encrypt(passWord)}
                };

                var json = new JavaScriptSerializer().Serialize(loginData);
                var input = new StringContent(json, Encoding.UTF8, "application/json");

                using (HttpResponseMessage resp = await client.PostAsync(Program.baseUrl + "/login", input))
                {
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();

                        if (resp.StatusCode == HttpStatusCode.OK)
                        {
                            UserRootObject user = new JavaScriptSerializer().Deserialize<UserRootObject>(data);

                            if (user.user.Count > 0)
                            {
                                return user.user[0].username;
                            }
                            else
                            {
                                MessageBox.Show("Incorrect Username or Password");
                            }
                        }
                        else
                        {
                            MessageBox.Show("An error ocurred during the registration");
                        }                     
                        return String.Empty;
                    }
                }
            }
        }
        //Get na API para trazer os dados do usuário
        public static async Task<UserData> GetUser(string userName)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage resp = await client.GetAsync(Program.baseUrl + "/user/get/" + userName))
                {
                    using (HttpContent content = resp.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        UserRootObject user = new JavaScriptSerializer().Deserialize<UserRootObject>(data);

                        if (user.user.Count > 0) {
                            return user.user[0];
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        //Faz o Encrypt MD5 na senha do usuário
        private static string Encrypt(string password)
        {
            string hash = "Proxion";
            byte[] data = UTF8Encoding.UTF8.GetBytes(password);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            TripleDESCryptoServiceProvider tripDES = new TripleDESCryptoServiceProvider();

            tripDES.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            tripDES.Mode = CipherMode.ECB;

            ICryptoTransform transform = tripDES.CreateEncryptor();
            byte[] result = transform.TransformFinalBlock(data, 0, data.Length);

            return Convert.ToBase64String(result);
        }
    }
}
